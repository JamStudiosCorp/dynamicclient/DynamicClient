## Permissions
- ✔️ Modification
- ✔️ Private Use
- ✔️ Distribution
## Limitations
- ❌ Commercial Use (unless if ModCreator gave Permission)
- ❌ Liability
- ❌ Warranty
## Conditions
- ℹ License and Copywrite Notice
- ℹ Same License (must be up-to-date)
- ℹ Credit External Developers (ModCreators)
